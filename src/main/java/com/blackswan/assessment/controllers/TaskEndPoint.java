package com.blackswan.assessment.controllers;

import com.blackswan.assessment.persistence.Task;
import com.blackswan.assessment.persistence.User;
import com.blackswan.assessment.persistence.repositories.TaskRepository;
import com.blackswan.assessment.persistence.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * This class is a REST controller for the Task object.
 *
 * Example below:
 * {
 *  "id"          : 999,
 *  "name"        : "value",
 *  "description" : "value",
 *  "date_time"   : "value"}
 *
 * Actions:
 *
 * POST /api/user/{user_id}/task            -> Creates a new task
 * PUT  /api/user/{user_id}/task/{task_id}  -> Updates an existing task
 * GET  /api/user/{user_id}/task/{task_id}  -> Retrieve a task
 * GET  /api/user/{user_id}/task            -> List all ta
 */
@RestController
@RequestMapping("/api/user/{userId}/task")
public class TaskEndPoint {

    @Autowired
    TaskRepository taskRepository;

    @Autowired
    UserRepository userRepository;

    /**
     * Create a Task
     * @param task path variable declared in class-level RequestMapping
     * @return the task that has just been created
     */
    @RequestMapping(value="", method= RequestMethod.POST, produces = "application/json")
    public Task createTask(@RequestBody Task task) {
        return taskRepository.save(task);
    }

 /**
  * Return a list of tasks for the user
  * @param userId the userId for whom you would like the tasks to be listed
  * @return a list of tasks
  */
 @RequestMapping(value="", method= RequestMethod.GET, produces = "application/json")
    public Set<Task> listTasks(@PathVariable("userId") Long userId) {
        User user = userRepository.getOne(userId);
        return taskRepository.findByUser(user);
    }

}
