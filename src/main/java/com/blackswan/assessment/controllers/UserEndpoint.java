package com.blackswan.assessment.controllers;

import com.blackswan.assessment.persistence.User;
import com.blackswan.assessment.persistence.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * This class is a REST controller for the User object.
 *
 * Example below:
 * {
 *  "id"         : 999,
 *  "username"   : "value",
 *  "first_name" : "value",
 *  "last_name"  : "value"
 *  }
 *
 * Actions:
 *
 * POST /    -> Creates a new user
 * PUT /{id} -> Updates an existing user
 * GET /{id} -> Retrieve a user
 * GET /     -> List all users
 */
@RestController
@RequestMapping ("/api/user")
public class UserEndpoint {

    @Autowired
    UserRepository userRepository;

    /**
     * Creates a user
     * @param user a user sent as JSON
     * @return the user you just created, with an id.
     */
    @RequestMapping(value="", method=RequestMethod.POST, produces = "application/json")
    public User createUser(@RequestBody User user) {
        return userRepository.save(user);
    }

    /**
     * Return a list of users
     * @return a list of users in JSON as an array
     */
    @RequestMapping(value="", method=RequestMethod.GET, produces = "application/json")
    public Set<User> listUsers() {
        return userRepository.findByOrderByFirstNameAsc();
    }

    /**
     * Exception handler for this controller
     * @param ex the exception to be caught. In this case, all exceptions
     * @return an error message in JSON.
     */
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public String handleException(Exception ex) {
        System.out.println("ERROR: " + ex.getMessage());
        if (ex.getMessage().toLowerCase().contains("constraint")) {
            return "{\"error\":\"There was an SQL Error. Most likely a duplicate user_name or id\"}";
        }
        return "{error:\"Unknown Error\")";
    }



    /*
    Create user

    curl -i -H "Content-Type: application/json" -X POST -d '{"username":"jsmith","first_name" : "John", "last_name" : "Smith"}' http://localhost:8080/api/user

    Update user

    curl -i -H "Content-Type: application/json" -X PUT -d '{"first_name" : "John", "last_name" : "Doe"}' http://localhost:8080/api/user/{id}

    List all users

    curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X GET http://localhost:8080/api/user

    Get User info

    curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X GET http://localhost:8080/api/user/{id}

    Create Task

    curl -i -H "Content-Type: application/json" -X POST -d '{"name":"My task","description" : "Description of task", "date_time" : "2016-05-25 14:25:00"}' http://localhost:8080/api/user/{user_id}/task

    Update Task

    curl -i -H "Content-Type: application/json" -X PUT -d '{"name":"My updated task"}' http://localhost:8080/api/user/{user_id}/task/{task_id}

    Delete Task

    curl -i -H "Content-Type: application/json" -X DELETE http://localhost:8080/api/user/{user_id}/task/{task_id}

    Get Task Info

    curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X GET http://localhost:8080/api/user/{user_id}/task/{task_id}

    List all tasks for a user

    curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X GET http://localhost:8080/api/user/{user_id}/task
    */


}
