package com.blackswan.assessment.persistence.repositories;

import com.blackswan.assessment.persistence.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

public interface UserRepository extends CrudRepository<User, Long> {

    Set<User> findAll();
    Set<User> findByOrderByFirstNameAsc();
    User getOne(Long id);

}
