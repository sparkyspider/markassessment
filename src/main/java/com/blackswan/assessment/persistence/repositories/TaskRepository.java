package com.blackswan.assessment.persistence.repositories;

import com.blackswan.assessment.persistence.Task;
import com.blackswan.assessment.persistence.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

public interface TaskRepository extends CrudRepository<Task, Long> {

    Set<Task> findAll();
    Set<Task> findByUser(User user);

}