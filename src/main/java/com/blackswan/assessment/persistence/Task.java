package com.blackswan.assessment.persistence;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.Date;

/***
 * Persistence object for a Task. Task belongs to a user.
 */
@Entity
public class Task {

    @Id @GeneratedValue(strategy = GenerationType.SEQUENCE)
    Long id;

    @ManyToOne
    User user;

    String name;

    String description; // This should be text

    @JsonProperty("date_time")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    Date dateTime;

    /** Constructors ***/

    protected Task() {

    }

    /*** Getters and Setters ***/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }
}
